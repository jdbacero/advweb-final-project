<?php
session_start();
if (!isset($_SESSION['username']) || $_SESSION['usertype'] != "Admin")
  header("location: index.php");
?>

<!DOCTYPE html>
<html>
<head>
  <link rel="stylesheet" type="text/css" href="css/materialize.min.css">
  <link rel="stylesheet" type="text/css" href="css/style.css">
  <link rel="stylesheet" type="text/css" href="css/admin.css">
  <link rel="stylesheet" type="text/css" href="css/tablepagination.css">
  <link rel="stylesheet" type="text/css" href="icon/icon.css">

  <style type="text/css">
    
    div.material-table table tr td {
      height: 35px;
    }

    div.material-table .table-footer {
      height: 40px;
    }

    .row {
      margin-bottom: 0px;
    }

  </style>

  <meta charset="utf-8">
  <title>Admin</title>
</head>
<body>

<div class="wrapper">
 <nav class="nav-extended">
    <div class="nav-wrapper">
      <span class=""><a href="#" class="admin">Admin</a></span>
      <a href="#" class="brand-logo center">Quiz Management System</a>
      <ul id="nav-mobile" class="right hide-on-med-and-down">
        <li><a class="waves-effect waves-light btn" id="btnLogout" href="javascript:(logout());">Logout</a></li>
      </ul>
    </div>
    <div class="nav-content">
      <ul class="tabs row tabs-transparent red lighten-1">
        <li class="tab"><a class="active" href="#test1">Manage Courses</a></li>
        <li class="tab" id="tabStudent"><a href="#test2">Manage Students</a></li>
        <li class="tab" id="tabAccount"><a href="#test3">Manage Accounts</a></li>
        <li class="tab" id="tabSubject"><a href="#test4">Manage Subjects</a></li>
        <li class="tab" id="tabTeacher"><a href="#test5">Manage Teachers</a></li>
        <li class="tab" id="tabCourseSub"><a href="#test6">Manage Course Subjects</a></li>
        <li class="tab" id="tabClass"><a href="#test7">Classes</a></li>
        <li class="tab" id="tabLogs"><a href="#test8">User Logs</a></li>
      </ul>
    </div>
  </nav>

  <!-- Tab for courses -->
  <?php include('./preloader_circle_blue.php'); ?>
  <div id="test1" class="col m12">
    
    <div class="adminUploadableTableContainer"><?php include('admin_tables/tbl_courses.php'); ?></div>


      <form method = "POST" enctype="multipart/form-data" action="upload.php"><br>

        <div align="center" id="test">

          <label class="browse waves-effect waves btn" id="browse"><input accept="Excel/*.xls*.xlsx, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" type="file" id="btnBrowseCourse" name="btnBrowseCourse" align="center" class="waves-effect waves-red btn-flat">Browse</label>
<!--       <form action="#">
        <div class="file-field input-field">
          <div class="btn">
            <span>Browse</span>
              <input  type="file" name="btnBrowseCourse">
          </div>
              <div class="file-path-wrapper">
                <input class="file-path validate" type="text">
              </div>
          </div>
        </form> -->

          <button class="upload waves-effect waves-red btn" type="submit" id="btnUploadCourse" name="btnUploadCourse">Upload</button>

        </div>

        <!-- button for downloading excel format -->
        <br><div align="center"><button class="waves-effect waves-light btn" name="downloadFrmCourse" formaction="download_Format.php">Download Format</button></div>
      </form>

  </div>

  <!-- Tab for students -->
  <div id="test2" class="col m12">

    <div class="adminUploadableTableContainer"><?php include('admin_tables/tbl_students.php'); ?></div>  

    <form method = "POST" enctype="multipart/form-data" action="upload.php"><br>

        <div align="center">

          <label class="browse waves-effect waves btn" id="browse"><input accept="Excel/*.xls*.xlsx, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" type="file" name="btnBrowseStudent" align="center" class="waves-effect waves-red btn-flat">Browse</label>

          <button class="upload waves-effect waves-red btn" type="submit" name="btnUploadStudent">Upload</button>

        </div>

        <!-- button for downloading excel format -->
        <br><div align="center"><button class="waves-effect waves-light btn" name="downloadFrmStudent" formaction="download_Format.php">Download Format</button></div>
    </form>

  </div>
  <div id="test3" class="col m12">

    <div class="adminUploadableTableContainer"><?php include('admin_tables/tbl_accounts.php'); ?></div>  

  </div>

  <!-- Tab for subjects -->
  <div id="test4" class="col m12">
    
    <div class="adminUploadableTableContainer"><?php include('admin_tables/tbl_subjects.php'); ?></div>

      <form method = "POST" enctype="multipart/form-data" action="upload.php"><br>

        <div align="center">

          <label class="browse waves-effect waves btn" id="browse"><input accept="Excel/*.xls*.xlsx, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" type="file" name="btnBrowseSubject" align="center" class="waves-effect waves-red btn-flat">Browse</label>

          <button class="upload waves-effect waves-red btn" type="submit" name="btnUploadSubject">Upload</button>

        </div>

        <!-- button for downloading excel format -->
        <br><div align="center"><button class="waves-effect waves-light btn" name="downloadFrmSubject" formaction="download_Format.php">Download Format</button></div>
      </form>

  </div>

  <!-- Tab for teachers -->
  <div id="test5" class="col m12">

    <div class="adminUploadableTableContainer"><?php include('admin_tables/tbl_teachers.php'); ?></div>
    <form method = "POST" enctype="multipart/form-data" action="upload.php"><br>

        <div align="center">

          <label class="browse waves-effect waves btn" id="browse"><input accept="Excel/*.xls*.xlsx, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" type="file" name="btnBrowseTeacher" align="center" class="waves-effect waves-red btn-flat">Browse</label>

          <button class="upload waves-effect waves-red btn" type="submit" name="btnUploadTeacher">Upload</button>

        </div>

        <!-- button for downloading excel format -->
        <br><div align="center"><button class="waves-effect waves-light btn" name="downloadFrmTeacher" formaction="download_Format.php">Download Format</button></div>
      </form>  

  </div>

  <!-- Tab for course subjects -->
  <div id="test6" class="col m12">

    <div class="adminUploadableTableContainer"><?php include('admin_tables/tbl_coursesubjects.php'); ?></div>
    <form method = "POST" enctype="multipart/form-data" action="upload.php"><br>

        <div align="center">

          <label class="browse waves-effect waves btn" id="browse"><input accept="Excel/*.xls*.xlsx, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" type="file" name="btnBrowseCourseSubject" align="center" class="waves-effect waves-red btn-flat">Browse</label>

          <button class="upload waves-effect waves-red btn" type="submit" name="btnUploadCourseSubject">Upload</button>

        </div>

        <!-- button for downloading excel format -->
        <br><div align="center"><button class="waves-effect waves-light btn" name="downloadFrmCourseSubject" formaction="download_Format.php">Download Format</button></div>
      </form>  

  </div>
    
  <!-- Tab for classes -->
  <div id="test7" class="col m12">
    <div class="adminUploadableTableContainer"><?php include('admin_tables/tbl_class.php'); ?></div>  
  </div>

  <!-- Tab for user logs -->
  <div id="test8" class="col m12">
      <?php include('admin_tables/tbl_userlogs.php'); ?>     
  </div>

</div>

</body>


<!--Import jQuery before materialize.js-->
<script src="js/jquery-3.2.1.min.js"></script>
<script type="text/javascript" src="js/jquery.dataTables.min.js"></script>
<script src='js/materialize.min.js'></script>
<script type="text/javascript" src="js/index.js"></script>
<script type="text/javascript" src="js/pagination.js"></script>
<script type="text/javascript" src="js/admin.js"></script>
<script type="text/javascript">checkEnaDis()</script>
</html>
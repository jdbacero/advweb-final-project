<?php

require_once 'Classes/PHPExcel.php';

// Create new Spreadsheet object
$spreadsheet = new PHPExcel();


// Set document properties
$spreadsheet->getProperties()->setCreator('Online Quiz Management System')
    ->setLastModifiedBy('Online Quiz Management System')
    ->setTitle('Online Quiz Management System')
    ->setSubject('Online Quiz Management System')
    ->setDescription('Online Quiz Management System')
    ->setKeywords('Online Quiz Management System')
    ->setCategory('file format');

if (isset($_POST['downloadFrmCourse'])) {
    # code...
    // Set column width
    $spreadsheet->getActiveSheet()->getColumnDimension('A')->setWidth(20);
    $spreadsheet->getActiveSheet()->getColumnDimension('B')->setWidth(50);
    // Add some data
    $spreadsheet->setActiveSheetIndex(0)
        ->setCellValue('A1', 'Course ID')
        ->setCellValue('B1', 'Course Name')
        ->setCellValue('A2', 'BSIT')
        ->setCellValue('B2', 'Bachelor of Science in Information Technology');

    // Rename worksheet
    $spreadsheet->getActiveSheet()->setTitle('Format');

    // Set active sheet index to the first sheet, so Excel opens this as the first sheet
    $spreadsheet->setActiveSheetIndex(0);


    // Redirect output to a client’s web browser (Xlsx)
    header('Content-Type: application/vnd.ms-excel');
    header('Content-Disposition: attachment;filename="Online Quiz Management System - Course Format.xls"');
    header('Cache-Control: max-age=0');
    // If you're serving to IE 9, then the following may be needed
    header('Cache-Control: max-age=1');

    // If you're serving to IE over SSL, then the following may be needed
    header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
    header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
    header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
    header ('Pragma: public'); // HTTP/1.0

    $writer = PHPExcel_IOFactory::createWriter($spreadsheet, 'Excel5');
    $writer->save('php://output');
    exit;
}

if (isset($_POST['downloadFrmStudent'])) {
    // Set column width
    $spreadsheet->getActiveSheet()->getColumnDimension('A')->setWidth(20);
    $spreadsheet->getActiveSheet()->getColumnDimension('B')->setWidth(20);
    $spreadsheet->getActiveSheet()->getColumnDimension('C')->setWidth(20);
    $spreadsheet->getActiveSheet()->getColumnDimension('D')->setWidth(20);
    $spreadsheet->getActiveSheet()->getColumnDimension('E')->setWidth(20);

    $spreadsheet->setActiveSheetIndex(0)
        ->setCellValue('A1', 'Section')
        ->setCellValue('B1', 'First Name')
        ->setCellValue('C1', 'Middle Name')
        ->setCellValue('D1', 'Last Name')
        ->setCellValue('E1', 'Course')
        ->setCellValue('A2', 'Section-A')
        ->setCellValue('B2', 'Jorex')
        ->setCellValue('C2', 'Gwapo')
        ->setCellValue('D2', 'Kaayo')
        ->setCellValue('E2', 'BSIT');

    // Rename worksheet
    $spreadsheet->getActiveSheet()->setTitle('Format');

    // Set active sheet index to the first sheet, so Excel opens this as the first sheet
    $spreadsheet->setActiveSheetIndex(0);


    // Redirect output to a client’s web browser (Xlsx)
    header('Content-Type: application/vnd.ms-excel');
    header('Content-Disposition: attachment;filename="Online Quiz Management System - Student Format.xls"');
    header('Cache-Control: max-age=0');
    // If you're serving to IE 9, then the following may be needed
    header('Cache-Control: max-age=1');

    // If you're serving to IE over SSL, then the following may be needed
    header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
    header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
    header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
    header ('Pragma: public'); // HTTP/1.0

    $writer = PHPExcel_IOFactory::createWriter($spreadsheet, 'Excel5');
    $writer->save('php://output');
    exit;
}

if (isset($_POST['downloadFrmTeacher'])) {
    // Set column width
    $spreadsheet->getActiveSheet()->getColumnDimension('A')->setWidth(20);
    $spreadsheet->getActiveSheet()->getColumnDimension('B')->setWidth(20);
    $spreadsheet->getActiveSheet()->getColumnDimension('C')->setWidth(20);

    $spreadsheet->setActiveSheetIndex(0)
        ->setCellValue('A1', 'First Name')
        ->setCellValue('B1', 'Middle Name')
        ->setCellValue('C1', 'Last Name')
        ->setCellValue('A2', 'Jorex')
        ->setCellValue('B2', 'Gwapo')
        ->setCellValue('C2', 'Kaayo');

    // Rename worksheet
    $spreadsheet->getActiveSheet()->setTitle('Format');

    // Set active sheet index to the first sheet, so Excel opens this as the first sheet
    $spreadsheet->setActiveSheetIndex(0);


    // Redirect output to a client’s web browser (Xlsx)
    header('Content-Type: application/vnd.ms-excel');
    header('Content-Disposition: attachment;filename="Online Quiz Management System - Teacher Format.xls"');
    header('Cache-Control: max-age=0');
    // If you're serving to IE 9, then the following may be needed
    header('Cache-Control: max-age=1');

    // If you're serving to IE over SSL, then the following may be needed
    header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
    header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
    header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
    header ('Pragma: public'); // HTTP/1.0

    $writer = PHPExcel_IOFactory::createWriter($spreadsheet, 'Excel5');
    $writer->save('php://output');
    exit;
}

if (isset($_POST['downloadFrmSubject'])) {
    // Set column width
    $spreadsheet->getActiveSheet()->getColumnDimension('A')->setWidth(30);
    $spreadsheet->getActiveSheet()->getColumnDimension('B')->setWidth(20);

    $spreadsheet->setActiveSheetIndex(0)
        ->setCellValue('A1', 'Subject Name')
        ->setCellValue('B1', 'Subject Code')
        ->setCellValue('A2', 'Advanced Web Principles')
        ->setCellValue('B2', 'ADVWEB');

    // Rename worksheet
    $spreadsheet->getActiveSheet()->setTitle('Format');

    // Set active sheet index to the first sheet, so Excel opens this as the first sheet
    $spreadsheet->setActiveSheetIndex(0);


    // Redirect output to a client’s web browser (Xlsx)
    header('Content-Type: application/vnd.ms-excel');
    header('Content-Disposition: attachment;filename="Online Quiz Management System - Subject Format.xls"');
    header('Cache-Control: max-age=0');
    // If you're serving to IE 9, then the following may be needed
    header('Cache-Control: max-age=1');

    // If you're serving to IE over SSL, then the following may be needed
    header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
    header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
    header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
    header ('Pragma: public'); // HTTP/1.0

    $writer = PHPExcel_IOFactory::createWriter($spreadsheet, 'Excel5');
    $writer->save('php://output');
    exit;
}

if (isset($_POST['downloadFrmCourseSubject'])) {
    // Set column width
    $spreadsheet->getActiveSheet()->getColumnDimension('A')->setWidth(20);
    $spreadsheet->getActiveSheet()->getColumnDimension('B')->setWidth(20);

    $spreadsheet->setActiveSheetIndex(0)
        ->setCellValue('A1', 'Subject')
        ->setCellValue('B1', 'Course')
        ->setCellValue('A2', 'ADVWEB')
        ->setCellValue('B2', 'BSIT');

    // Rename worksheet
    $spreadsheet->getActiveSheet()->setTitle('Format');

    // Set active sheet index to the first sheet, so Excel opens this as the first sheet
    $spreadsheet->setActiveSheetIndex(0);


    // Redirect output to a client’s web browser (Xlsx)
    header('Content-Type: application/vnd.ms-excel');
    header('Content-Disposition: attachment;filename="Online Quiz Management System - Subject Format.xls"');
    header('Cache-Control: max-age=0');
    // If you're serving to IE 9, then the following may be needed
    header('Cache-Control: max-age=1');

    // If you're serving to IE over SSL, then the following may be needed
    header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
    header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
    header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
    header ('Pragma: public'); // HTTP/1.0

    $writer = PHPExcel_IOFactory::createWriter($spreadsheet, 'Excel5');
    $writer->save('php://output');
    exit;
}

if (isset($_POST['downloadFrmQuiz'])) {
    // Set column width
    $spreadsheet->getActiveSheet()->getColumnDimension('A')->setWidth(40);
    $spreadsheet->getActiveSheet()->getColumnDimension('B')->setWidth(30);
    $spreadsheet->getActiveSheet()->getColumnDimension('C')->setWidth(30);
    $spreadsheet->getActiveSheet()->getColumnDimension('D')->setWidth(30);
    $spreadsheet->getActiveSheet()->getColumnDimension('E')->setWidth(30);
    $spreadsheet->getActiveSheet()->getColumnDimension('F')->setWidth(30);

    $spreadsheet->setActiveSheetIndex(0)
        ->setCellValue('A1', 'Questions')
        ->setCellValue('B1', 'Option A')
        ->setCellValue('C1', 'Option B')
        ->setCellValue('D1', 'Option C')
        ->setCellValue('E1', 'Option D')
        ->setCellValue('F1', 'Correct Answer')
        ->setCellValue('A2', 'What does "WWW" stand for?')
        ->setCellValue('B2', 'World Worm Web')
        ->setCellValue('C2', 'World Wide Web')
        ->setCellValue('D2', 'World Word Web')
        ->setCellValue('E2', 'None of these')
        ->setCellValue('F2', 'World Wide Web');

    // Rename worksheet
    $spreadsheet->getActiveSheet()->setTitle('Format');

    // Set active sheet index to the first sheet, so Excel opens this as the first sheet
    $spreadsheet->setActiveSheetIndex(0);


    // Redirect output to a client’s web browser (Xlsx)
    header('Content-Type: application/vnd.ms-excel');
    header('Content-Disposition: attachment;filename="Online Quiz Management System - Quiz Format.xls"');
    header('Cache-Control: max-age=0');
    // If you're serving to IE 9, then the following may be needed
    header('Cache-Control: max-age=1');

    // If you're serving to IE over SSL, then the following may be needed
    header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
    header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
    header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
    header ('Pragma: public'); // HTTP/1.0

    $writer = PHPExcel_IOFactory::createWriter($spreadsheet, 'Excel5');
    $writer->save('php://output');
    exit;
}


<?php

$con = mysqli_connect('localhost', 'root', '', 'final_proj');
$subject = array();
#isud ug array para gamiton sa dropdown/select as items nila

$query = "SELECT * FROM tblsubjects ORDER BY Subject_Code ASC";
$result = mysqli_query($con, $query);

while($data = mysqli_fetch_array($result)) {
	array_push($subject, $data['Subject_Code']);
}


?>

<div id="quizModal" class="modal">
	<div class="modal-content">
		<h4 align="center">Upload and Create Quizzes</h4><hr/>
		<div class="row">
			<!-- select quiz -->
			<form class="col s12" method="POST" enctype="multipart/form-data">
				<div class="file-field input-field">
					<div class="btn">
						<span>Browse</span>
					    <input name="fpQuiz" type="file">
					</div>
			      	<div class="file-path-wrapper">
				        <input class="file-path validate" type="text">
			      	</div>
			    </div>
				<!-- quiz name -->
				<div class="input-field col s12">
					<input id="quizID" name="quizName" type="text" class="validate" required>
					<label for="quizID">Quiz Name</label>
				</div>
			  	<!-- quiz subject -->
			  	<div class="input-field col s6">
			    	<select name="subject" id="selectSub" onchange="onSetSubject()">
			      		<option value="" disabled selected>Subject</option>
						<!-- <option value="1">Option 1</option>
			      		<option value="2">Option 2</option>
			      		<option value="3">Option 3</option> -->
			      		<?php

						foreach($subject as $arr) {
								echo "<option>$arr</option>";	
							}
			      			
			      		?>
			    	</select>
			  	</div>
				<!-- quiz section -->
				<div class="input-field col s6">
			    	<select name="section" id="section" disabled>
			      		<option value="" disabled selected>Section</option>
			      		<option id="selectPlaceholder" class="sectionThing">Placeholder</option>
			      		

			    	</select>
			  	</div>
				<!-- quiz duration -->
			  	<div class="input-field col s6">
					<input id="durationID" type="text" class="validate" name="duration" required onkeypress="return isNumberKey(event)">
					<label for="durationID">Duration (minutes)</label>
				</div>
				<!-- quiz attempts  -->
				<div class="input-field col s6">
					<input id="attemptID" type="text" name="attempts" class="validate" required onkeypress="return isNumberKey(event)"> 
					<label for="attemptID">Attempts Available</label>
				</div>
				<!-- quiz availability -->
				<div class="col s6">
					<input type="text" class="datepicker" id="dateStart" name="dateStart" placeholder="Start Availability" onchange="onSetDate()" >
				</div>
				<!-- quiz deadline -->
				<div class="col s6">
					<input type="text" class="datepicker" id="dateEnd" name="dateEnd" placeholder="Deadline" disabled>
				</div>
				<!-- quiz upload -->
				<div class="col s12">
					<button class="upload waves-effect waves-light btn col s2 offset-s3" name="btnUploadQuiz" id="uploadQuiz" formaction="upload.php">Upload</button>
					<button class="waves-effect waves-light btn col s2 offset-s2" formnovalidate name="downloadFrmQuiz" id="downloadFormat" formaction="download_Format.php">Create</button>
				</div>		
		  	</form>		
		</div>
	</div>
</div>
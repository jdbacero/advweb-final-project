<?php

//Column Header Checker
# function columnHeaderChecker()	
//pseudocode
	/*

	#nilapas ang supposed number of  column headers
	if(noOfCol > columnheader.count) {
		echo "Columns overlapped";
	}

	*/#

#Actual working code --returns false if naay inconsitency sa giupload nga excel file.
function contentChecker($noOfCol, $highestColumn, array $columnHeader, $noOfRow) {

	if($highestColumn > $noOfCol){
		echo 'Columns overlapped.';
		return false;
	} else if ($highestColumn < $noOfCol) {
		echo 'Insufficient columns.';
		return false;
	}

	global $objPHPExcel;

	for($i = 1, $letter = 'A'; $i <= count($columnHeader); $i++, $letter++) {

		$cellValue = $objPHPExcel->getActiveSheet()->getCell($letter . '1')->getValue();

		if(strtolower($cellValue) != strtolower($columnHeader[$i - 1])) {
			echo 'Invalid. It seems there\'s an error in the formatting. Are the header names correct?';
			return false;
			break;
		}
	}

	#This loop-within-a-loop code checks if there's an empty cell
	for($x = 2; $x <= $noOfRow; $x++) {
		
		for($i = 1, $letter = 'A'; $i <= count($columnHeader); $i++, $letter++){
			$cellValue = $objPHPExcel->getActiveSheet()->getCell($letter . $x)->getValue();

			if (empty($cellValue)) {
				echo 'Invalid. It seems there\'s an error in the formatting. Are/Is there any blank cell/s in the excel file?';
				return false;
			}

			if($i == count($columnHeader)){
				$letter = 'A';
			}
		}
	}

	return true;

}

#Function for uploading
function uploadExcel(array $columnHeader, $noOfRow, $con, $incrementation, $tblname, $noOfFields){

	#This array variable will serve as the container for every data in a cell
	$data = array();

	#This is a partial query.
	$query = "INSERT INTO $tblname VALUES (";
	$ctr = 0;

	#This is to get the $objPHPExcel variable in the upload.php
	global $objPHPExcel;

	#This loop is to set every data in the excel in the array
	for($x = 2; $x <= $noOfRow; $x++) {
		
		for($i = 1, $letter = 'A'; $i <= count($columnHeader); $i++, $letter++){
			$cellValue = $objPHPExcel->getActiveSheet()->getCell($letter . $x)->getValue();
			array_push($data, $cellValue);
			if($i == count($columnHeader)){
				$letter = 'A';
			}

			// mysqli_query($con, $query);

		}
	}

	#Tangtangon ra ni. This is to check if nasud ba jud ang data sa array.
	// foreach($data as $arr) {
	// 	echo $arr;
	// }

	#Remember the partial query? This loop is used to append the partial query para automated kunohay, haha.
	for ($x = 2; $x <= $noOfRow; $x++) {

		for ($j = 0; $j < $noOfFields; $j++, $ctr++) {
			if (($noOfFields-$j) == 1) {
				$query .= "'$data[$ctr]') ";
			} else {
				$query .= "'$data[$ctr]', ";
			}

		}
	
	mysqli_query($con, $query);

	#Tangtangon ra ni. This is to check if sakto ba jud ang pagka-append ba jud ang query through the for-loop.
	//echo $query; di na mugana kay AJAX gamit

	#Para ma reset ang query back to its partial value for every loop.
	$query = "INSERT INTO $tblname VALUES (";
	}
	
	echo 'Uploaded successfully';

}

#Uploads master list of students. Process is: 
#1) Get every section with their corresponding course and insert it in tblcourse 
#2) Get the first 2 - 4 letters of their first name and last name(varies depending on the number of characters of their firstname and lastname) and that will be their username. Default password will be 123456. Data is then inserted in tbllogin.
#3) Insert all data in tblstudent_details
function uploadStudents($noOfRow, $con) {

	#Multi-Dimensional Array for student data
	$studentData = array(

		/*0 "Section" */ array(),
		/*1 "First Name"  */ array(),
		/*2 "Middle Name" */ array(),
		/*3 "Last Name" */ array(),
		/*4 "Course" */ array()

	);

	#This is to get the $objPHPExcel variable in the upload.php
	global $objPHPExcel;

	#Loop for inserting data in multidimensional array
	for ($i=2; $i <= $noOfRow; $i++) { 
		
		for ($z = 0, $letter = 'A'; $letter <= 'E'; $letter++, $z++) { 

			$cellValue = $objPHPExcel->getActiveSheet()->getCell($letter . $i)->getValue();
			$studentData[$z][] = $cellValue;

		}

	}

	#Insert tblclass
	$ctrClassSection = 0;
	foreach($studentData[0] as $arr) {
		$query = "INSERT INTO tblclass VALUES ('".$studentData[0][$ctrClassSection]."', '".$studentData[4][$ctrClassSection]."');";
		mysqli_query($con, $query);
		$ctrClassSection++;
	}

	#Making of accounts
	$ctrAccounts = 0;
	foreach($studentData[0] as $arr) {

		$username;

		#Determining what username should be
		if (strlen($studentData[1][$ctrAccounts]) < 3 && strlen($studentData[3][$ctrAccounts]) > 3) {

			$username = substr($studentData[1][$ctrAccounts], 0, 2) . '.' . substr($studentData[3][$ctrAccounts], 0, 4);

		} else if (strlen($studentData[1][$ctrAccounts]) < 3 && strlen($studentData[3][$ctrAccounts]) < 3) {

			$username = substr($studentData[1][$ctrAccounts], 0, 2) . '.' . substr($studentData[3][$ctrAccounts], 0, 2);

		} else if (strlen($studentData[1][$ctrAccounts]) > 3 && strlen($studentData[3][$ctrAccounts]) < 3) {

			$username = substr($studentData[1][$ctrAccounts], 0, 4) . '.' . substr($studentData[3][$ctrAccounts], 0, 2);

		} else {

			$username = substr($studentData[1][$ctrAccounts], 0, 3) . '.' . substr($studentData[3][$ctrAccounts], 0, 3);

		}

		$queryAcc = "INSERT INTO tbluser_login VALUES ('$username', '123456', 'Student');";
		$queryAddStud = "INSERT INTO tblstudents_details (Section, First_Name, Middle_Name, Last_Name, Course_ID, userType, userName) VALUES ('".$studentData[0][$ctrAccounts]."', '".$studentData[1][$ctrAccounts]."', '".$studentData[2][$ctrAccounts]."', '".$studentData[3][$ctrAccounts]."', '".$studentData[4][$ctrAccounts]."', 'Student', '$username');";
		mysqli_query($con, $queryAcc);
		mysqli_query($con, $queryAddStud);
		$ctrAccounts++;
	}

	echo 'Successfully uploaded.';
}

function uploadTeachers($noOfRow, $con) {

	#Multi-Dimensional Array for student data
	$teacherData = array(

		/*0 "First Name" */ array(),
		/*1 "Middle Name"  */ array(),
		/*2 "Last Name" */ array()

	);

	#This is to get the $objPHPExcel variable in the upload.php
	global $objPHPExcel;

	#Loop for inserting data in multidimensional array
	for ($i=2; $i <= $noOfRow; $i++) { 
		
		for ($z = 0, $letter = 'A'; $letter <= 'C'; $letter++, $z++) { 

			$cellValue = $objPHPExcel->getActiveSheet()->getCell($letter . $i)->getValue();
			$teacherData[$z][] = $cellValue;

		}

	}

	#Making of accounts
	$ctrAccounts = 0;
	foreach($teacherData[0] as $arr) {

		$username;

		#Determining what username should be
		if (strlen($teacherData[0][$ctrAccounts]) < 3 && strlen($teacherData[2][$ctrAccounts]) > 3) {

			$username = 't.' . substr($teacherData[0][$ctrAccounts], 0, 2) . '.' . substr($teacherData[2][$ctrAccounts], 0, 4);

		} else if (strlen($teacherData[0][$ctrAccounts]) < 3 && strlen($teacherData[2][$ctrAccounts]) < 3) {

			$username = 't.' . substr($teacherData[0][$ctrAccounts], 0, 2) . '.' . substr($teacherData[2][$ctrAccounts], 0, 2);

		} else if (strlen($teacherData[0][$ctrAccounts]) > 3 && strlen($teacherData[2][$ctrAccounts]) < 3) {

			$username = 't.' . substr($teacherData[0][$ctrAccounts], 0, 4) . '.' . substr($teacherData[2][$ctrAccounts], 0, 2);

		} else {

			$username = 't.' . substr($teacherData[0][$ctrAccounts], 0, 3) . '.' . substr($teacherData[2][$ctrAccounts], 0, 3);

		}

		$queryAcc = "INSERT INTO tbluser_login VALUES ('$username', '123456', 'Instructor');";
		$queryAddInst = "INSERT INTO tblteacher_details (First_Name, Middle_Name, Last_Name, userName, userType) VALUES ('".$teacherData[0][$ctrAccounts]."', '".$teacherData[1][$ctrAccounts]."', '".$teacherData[2][$ctrAccounts]."', '$username', 'Instructor');";
		mysqli_query($con, $queryAcc);
		mysqli_query($con, $queryAddInst);
		$ctrAccounts++;
	}

	echo 'Successfully uploaded.';

}

#what living hell awaits me developing this function
function uploadQuiz(array $quizDetails, $con, $noOfRow) {

	session_start();
	$teacher = $_SESSION['teacher'];
	$teacherID = $teacher['ID'];

	$quizDetails[5] = date('Y/m/d', strtotime($quizDetails[5]));
	$quizDetails[6] = date('Y/m/d', strtotime($quizDetails[6]));

	#Insert quiz details first, then insert quiz answers
	$queryQuizDetails = "INSERT INTO tblquiz_details (Subject_Code, Attempts_Available, Duration, Section, Quiz_Title, Quiz_Available, Quiz_Disable, Teacher_ID) VALUES ('".$quizDetails[2]."', '".$quizDetails[4]."', '".$quizDetails[3]."', '".$quizDetails[1]."', '".$quizDetails[0]."', '".$quizDetails[5]."', '".$quizDetails[6]."', '$teacherID');";

	mysqli_query($con, $queryQuizDetails);

	#Multi-Dimensional Array for quiz questions and answers
	$quizQuestionAnswer = array(

		/*0 "Question" */ array(),
		/*1 "A"  */ array(),
		/*2 "B"  */ array(),
		/*3 "C"  */ array(),
		/*4 "D"  */ array(),
		/*5 "Correct Answer" */ array()

	);

	######Getting Quiz ID#####

	$queryQuizId = "SELECT `AUTO_INCREMENT` FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'final_proj' AND TABLE_NAME = 'tblquiz_details'";

	$getQuizId = mysqli_query($con, $queryQuizId);
	$getResult = mysqli_fetch_assoc($getQuizId);
	$quizId = $getResult['AUTO_INCREMENT'] - 1;

	##########################

	#This is to get the $objPHPExcel variable in the upload.php
	global $objPHPExcel;

	for ($i=2; $i <= $noOfRow ; $i++) { 
		
		for ($x=0, $letter = 'A'; $letter <= 'F'; $x++, $letter++) { 
			
			$cellValue = $objPHPExcel->getActiveSheet()->getCell($letter . $i)->getValue();
			$quizQuestionAnswer[$x][] = $cellValue;

		}

	}

	$ctr = 0;
	foreach($quizQuestionAnswer[0] as $arr) {

		$queryInsertQuestion = "INSERT INTO tblQuiz_Questions VALUES ($quizId, '".$quizQuestionAnswer[0][$ctr]."', '".$quizQuestionAnswer[1][$ctr]."', '".$quizQuestionAnswer[2][$ctr]."', '".$quizQuestionAnswer[3][$ctr]."', '".$quizQuestionAnswer[4][$ctr]."', '".$quizQuestionAnswer[5][$ctr]."')";

		$ctr++;
		mysqli_query($con, $queryInsertQuestion);

	}

}

?>
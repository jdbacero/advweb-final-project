<?php session_start(); if (!isset($_SESSION['username']) || $_SESSION['usertype'] != "Instructor") header("location: index.php"); ?>

<?php 

$con = mysqli_connect('localhost', 'root', '', 'final_proj');
$section = array();
#isud ug array para gamiton sa dropdown/select as items nila

$query = "SELECT * FROM tblclass ORDER BY Section ASC";
$result = mysqli_query($con, $query);

while($data = mysqli_fetch_array($result)) {
	array_push($section, $data['Section']);
}
$teacher = $_SESSION['teacher'];
?> 

<!DOCTYPE html>
<html>
<head>
	<title>Instructor</title>
	<link rel="stylesheet" type="text/css" href="css/materialize.min.css">
	<link rel="stylesheet" type="text/css" href="css/tablepagination.css">
	<link rel="stylesheet" type="text/css" href="css/inStud.css">
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<link rel="stylesheet" type="text/css" href="icon/icon.css">
	<script type="text/javascript">
	function isNumberKey(evt){
    var charCode = (evt.which) ? evt.which : event.keyCode
    //kani na conodition if ginhan ka na naay decimal -> if (charCode > 31 && (charCode != 46 &&(charCode < 48 || charCode > 57))) 
    if (charCode > 31 && (charCode < 48 || charCode > 57))
        return false;

    return true;
}
	</script>
</head>
<body> 
	<!-- ibutang sa gawas sa div para mo hayag -->
	<!-- top navbar -->
	<div class="wrapper">
	<?php include('instModal.php') ?>
	<?php include('resetPasswordModal.php') ?>
	<?php include('editQuiz.php') ?>
		<nav class="nav-extended">
			<div class="nav-wrapper teal darken-1">
				<a href="#" class="brand-logo center">Instructor</a>
			</div>
			<div class="nav-content">
				<ul class="tabs row tabs-transparent teal lighten-1">
					<li class="tab col s6"><a href="#instQuiz">Quizzes</a></li>
					<li class="tab col s6"><a href="#instStud">Students</a></li>
				</ul>
			</div>
  		</nav>
	</div>
	<!-- top navbar tabs -->
  <?php include('./preloader_circle_blue.php'); ?>
	<div id="instQuiz" class="col m12"> <!-- tab for quizzes -->
		<!-- todo quizzes  -->
		<?php include('./instructor/generate/quizzes.php') ?>
		<!-- quiz modal trigger btn -->
		<div id="upQuiz" align="center">
			<a class="waves-effect waves-light btn modal-trigger" href="#quizModal">Upload Quiz</a>
		</div>
	</div>
		
	<!-- tab for studentss -->
	<div id="instStud" class="col m12">
		<div class="row">
			<div class="input-field col s3">
				<!-- dropdown for section -->
			    <select id="studSec">
		      		<option value="" disabled selected>Section</option>
		      		<?php 

		      		foreach ($section as $arr) {
		      			echo '<option>'.$arr.'</option>';
		      		}

		      		?>
		    	</select>
		  	</div>       
		</div>
		<div id="studPerformance">
			
		</div>
	</div>
	
	<!-- side nav -->
	<ul id="slide-out" class="side-nav fixed">
		<li>
			<div class="user-view">
				<div class="background">
					<img src="images/bg3.png">
				</div>
				<img class="circle" src="images/pic3.png"></a>
				<span class="black-text name"><?php echo $teacher['userName']; ?></span></a>
				<span class="black-text name"><?php echo $teacher['First_Name']. ' ' . $teacher['Middle_Name'][0] . '. ' . $teacher['Last_Name'] ; ?></span></a>
				<span class="black-text email">Instructor</span></a>
			</div>
		</li>
	    <li><button data-target="modal1" class="btn modal-trigger" style="width: 300px">Change Password</button></li>
	    <li><a class="waves-effect" href="logout.php">Logout</a></li>
	</ul>
	<!-- una jquery -->
	<script type="text/javascript" src="js/jquery-3.2.1.min.js"></script>
	<script type="text/javascript" src="js/jquery.dataTables.min.js"></script>
	<script type="text/javascript" src="js/pagination.js"></script>
	<script type="text/javascript" src="js/materialize.js"></script>
	<script type="text/javascript" src="js/index.js"></script>
	<script type="text/javascript" src="js/instModal.js"></script>
	<script type="text/javascript" src="js/inst.js"></script>
	<script type="text/javascript" src="js/updateQuiz.js"></script>
</body>
</html>


<?php include('indexheader.php') ?>

    <div class="row">
        <div class="col s12 m6 offset-m3">
            <div class="card">
                <div class="card-content">
                    <span class="card-title" style="text-align: center; font-size: 40px;">About us</span>
                    <span><br><br>
                        <span style="font-weight: bold; font-size: 25px;">Hello there!</span>
                            The Quiz Management System aims to create an easy and efficient program that creates, monitors. and lets students take different quizzes.
                            The sytem focuses on creating a well-defined exam/quiz for each individual departments. The system was created in order to give convenience in giving and taking of examinations in a educational organization, namely the users are the instructors and students.                 
                    </span>
                    <div class="card-title" style="text-align: center; font-size: 40px; margin-top: 50px;">Meet the Team</div><br><br>
                    <!-- the team -->
                    <div class="row">
                      <!-- jorx -->
                        <div class="col m7" style="margin-bottom: 50px;">
                            <img style="width: 50%; position: relative; left: 25%;" src="images/jorex.jpg">
                        </div>
                        <div class="col m5">
                            <span><h5>Jorex D. Bacero</h5></span>
                            <span>Team Leader (tig buhatstanan)</span><br>
                            <span>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                                tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                                quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                                consequat.
                            </span>
                        </div>
                        <!-- daniel -->
                        <div class="col m6">
                            <span><h5>Clyde Daniel J. Pableo</h5></span>
                            <span>Team Designer (tig correction)</span><br>
                            <span>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                            tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                            quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                            consequat.</span>
                        </div>
                        <div class="col m6" style="margin-bottom: 50px;">
                            <img style="width: 50%; position: relative; left: 25%;" src="images/clyde.jpg">
                        </div>
                        <!-- al -->
                        <div class="col m7">
                            <img style="width: 50%; position: relative; left: 25%;" src="images/al.jpg">
                        </div>
                        <div class="col m5"">
                            <span><h5>Al Joseph U. Monisit</h5></span>
                            <span>Team Debugger (tig tig)</span><br>
                            <span>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                                tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                                quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                                consequat.
                            </span>
                        </div>
                    </div> 
                </div>
            </div>
        </div>
    </div>
	<script type="text/javascript" src="js/jquery-3.2.1.min.js"></script>
	<script type="text/javascript" src="js/materialize.js"></script>
</body>
</html>
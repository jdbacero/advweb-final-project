<?php 
	session_start();
	$con = mysqli_connect('localhost', 'root', '', 'final_proj');
	$quizAnswers = $_SESSION['answers'];
	$userAnswers = $_SESSION['userChoices'];
	$student = $_SESSION['student'];
	$studentID = $student['ID'];
	$quizId = $_SESSION['quizID']; 
	$score = 0;

	for ($i=0; $i < count($quizAnswers) ; $i++) { 
		if ($quizAnswers[$i] == $userAnswers[$i]) {
			$score++;
		}
	}

	//this function checks if the user has taken the quiz before or not
	function firstOrNot($con, $quizId, $studentID) {
		$counter = "SELECT COUNT(*) AS Counter FROM tblQuiz_Results WHERE Quiz_ID = $quizId AND Student_ID = $studentID";
		$getCounter = mysqli_fetch_assoc(mysqli_query($con, $counter));
		return $getCounter['Counter'];
	}

	//If user has not taken the quiz before, insert quiz results and deduct number of attempts
	if(firstOrNot($con, $quizId, $studentID) == 0) {
		$queryInsertQuizResult = "INSERT INTO tblQuiz_Results (Quiz_ID, Student_ID, Score, Attempts) VALUES($quizId, $studentID, $score, 1)";
		mysqli_query($con, $queryInsertQuizResult);

	} else {
		$queryGetAttempts = "SELECT Attempts FROM tblQuiz_Results WHERE Quiz_ID = $quizId AND Student_ID = $studentID";
		$queryGetLastScore = "SELECT Score FROM tblQuiz_Results WHERE Quiz_ID = $quizId AND Student_ID = $studentID";
		$getAttempt = mysqli_fetch_assoc(mysqli_query($con, $queryGetAttempts));
		$getLastScore = mysqli_fetch_assoc(mysqli_query($con, $queryGetLastScore));
		$attempts = $getAttempt['Attempts'];
		$lastScore = $getLastScore['Score'];

		//If user has taken quiz before, do the following:

		//Updates score if new score is higher than previous score and deduct number of attempts
		if ($lastScore < $score) {
			$queryUpdateScoreAndAttempts = "UPDATE tblQuiz_Results SET Score = $score, Attempts = ($attempts + 1) WHERE Quiz_ID = $quizId AND Student_ID = $studentID";
			mysqli_query($con, $queryUpdateScoreAndAttempts);
	 	//Otherwise...
		//Just deduct number of attempts
		} else {
			$queryUpdateAttempts = "UPDATE tblQuiz_Results SET Attempts = ($attempts + 1) WHERE Quiz_ID = $quizId AND Student_ID = $studentID";
			mysqli_query($con, $queryUpdateAttempts);

		}
	}
	$_SESSION['quizStart'] = null;
	$_SESSION['lastTakenQuiz'] = true;
	echo "Your score is: " . $score . " / " . count($quizAnswers);
?>



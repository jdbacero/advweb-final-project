<div class="row">
  <div id="admin" class="col s12">
    <div class="material-table">
      <div class="table-header">
        <span class="table-title">Manage Quizzes</span>
        <div class="actions">
          <a href="#" class="search-toggle waves-effect btn-flat nopadding"><i class="material-icons">search</i></a>
        </div>
      </div>
      <table class="datatable highlight centered responsive-table">
        <thead>
          <tr>
              <th>Title</th>
              <th>Subject</th>
              <th>Attempts</th>
              <th>Availability</th>
              <th>Expires</th>
              <th></th>
          </tr>
        </thead>
        <tbody>
            <?php
            $teacherID = $_SESSION['teacher']['ID'];
            $con = mysqli_connect('localhost', 'root', '', 'final_proj');
      			$query = "SELECT tblQuiz_Details.Quiz_ID, tblQuiz_Details.Quiz_Title AS 'Title', tblQuiz_Details.Subject_Code AS 'Subject', tblQuiz_Details.Section, tblQuiz_Details.Attempts_Available AS 'Attempts', tblQuiz_Details.Duration, tblQuiz_Details.Quiz_Available AS 'Availability', tblQuiz_Details.Quiz_DISABLE AS 'Expires' FROM tblQuiz_Details WHERE tblQuiz_Details.Teacher_ID = $teacherID";
      			$result = mysqli_query($con,$query);

      			while ($row = mysqli_fetch_array($result)) {
      				echo '<tr>';
      				echo '<td>'.$row['Title'].'</td>';
              echo '<td>'.$row['Subject'].'</td>';
              echo '<td>'.$row['Attempts'].'</td>';
              echo '<td>'.$row['Availability'].'</td>';
              echo '<td>'.$row['Expires'].'</td>';
              echo '<td class="takeQuiz" id="'. $row['Quiz_ID'] .'">Edit</td>';
      				echo '</tr>';
              }
            ?>
        </tbody>
      </table>
    </div>
  </div>
</div>
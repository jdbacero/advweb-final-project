<?php 
session_start();
$teacherID = $_SESSION['teacher']['ID'];
$con = mysqli_connect('localhost', 'root', '', 'final_proj');
$section = $_POST['section'];
$query = "SELECT tblQuiz_Results.Quiz_ID, tblQuiz_Details.Quiz_Title, tblQuiz_Details.Subject_Code, tblQuiz_Details.Section AS 'Quiz_Section', tblStudents_Details.Last_Name, tblStudents_Details.First_Name, tblStudents_Details.Section AS 'Student_Section', tblQuiz_Results.Score, (SELECT COUNT(*) AS Expr1 FROM tblQuiz_Questions WHERE (Quiz_ID = tblQuiz_Results.Quiz_ID)) AS 'Overall' FROM tblQuiz_Results INNER JOIN tblStudents_Details ON tblQuiz_Results.Student_ID = tblStudents_Details.ID INNER JOIN tblQuiz_Details ON tblQuiz_Results.Quiz_ID = tblQuiz_Details.Quiz_ID	WHERE tblQuiz_Details.Teacher_ID = $teacherID AND tblStudents_Details.Section = '$section';";

$result = mysqli_query($con,$query);

echo '<div class="row">
  <div id="admin" class="col s12">
    <div class="material-table">
      <div class="table-header">
        <span class="table-title">Manage Students</span>
        <div class="actions">
          <a href="#" class="search-toggle waves-effect btn-flat nopadding"><i class="material-icons">search</i></a>
        </div>
      </div>
      <table class="datatable highlight centered responsive-table">
        <thead>
          <tr>
              <th>Quiz ID</th>
              <th>Quiz Title</th>
              <th>Subject</th>
              <th>Quiz\' Section</th>
              <th>Student</th>
              <th>Student\'s<br>Section</th>
              <th>Score</th>
              <th>Overall</th>
          </tr>
        </thead>
        <tbody>';

while ($row = mysqli_fetch_array($result)) {
	echo '<tr>';
	echo '<td>'.$row['Quiz_ID'].'</td>';
	echo '<td>'.$row['Quiz_Title'].'</td>';
	echo '<td>'.$row['Subject_Code'].'</td>';
	echo '<td>'.$row['Quiz_Section'].'</td>';
	echo '<td>'.$row['First_Name'].' '.$row['Last_Name'].'</td>';
	echo '<td>'.$row['Student_Section'].'</td>';
	echo '<td>'.$row['Score'].'</td>';
	echo '<td>'.$row['Overall'].'</td>';
	echo '</tr>';
  }

echo '</tbody>
      </table>
    </div>
  </div>
</div>';
        

?>

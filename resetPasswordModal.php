<!-- Modal Structure -->
<div id="modal1" class="modal modal-fixed-footer">
    <!-- <form> -->
      <div class="modal-content">
        <h4>Change Password</h4>
        <hr><br>
          <div class="input-field col s3 push-m3">
            <input id="oldPW" type="password" name="oldPW" class="validate" required>
            <label for="oldPW">Current Password</label>
          </div>
          <div class="input-field col s3 push-m3">
            <input id="newPW" type="password" class="validate" name="password" required>
            <label for="newPW">New Password</label>
          </div>
          <div class="input-field col s3 push-m3">
            <input id="renewPW" type="password" class="validate" name="repassword" required>
            <label for="renewPW">Confirm New Password</label>
          </div>
      </div>
      <div class="modal-footer">
<!--           <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat ">Reset Password</a> -->
            <button id="resetPW" class="btn">Reset</button>
      </div>
    <!-- </form> -->
</div>
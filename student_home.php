<?php session_start(); if (!isset($_SESSION['username']) || $_SESSION['usertype'] != "Student") header("location: index.php"); $student = $_SESSION['student']; ?>

<!DOCTYPE html>
<html>
<head>
	<title>Student</title>
	<link rel="stylesheet" type="text/css" href="css/materialize.min.css">
	<link rel="stylesheet" type="text/css" href="css/inStud.css">	
	<link rel="stylesheet" type="text/css" href="css/style.css">
  	<link rel="stylesheet" type="text/css" href="css/tablepagination.css">
	<link rel="stylesheet" type="text/css" href="icon/icon.css">
</head>
<body>
	<?php include('preloader_circle_blue.php'); ?>
	<!-- top navbar -->
	<div class="wrapper">
	<?php include('resetPasswordModal.php') ?>
		<nav class="nav-extended">
			<div class="nav-wrapper blue darken-1">
				<a href="#" class="brand-logo center">Student</a>
			</div>
			<div class="nav-content">
				<ul class="tabs row tabs-transparent blue lighten-1">
					<li class="tab col s6"><a href="#studQuiz">Quizzes</a></li>
					<li class="tab col s6"><a href="#studRes">Results</a></li>
				</ul>
			</div>
  		</nav>
	</div>
	<!-- navbar tabs -->
	<div id="studQuiz" class="col m12">
		<!--  todo quizzes -->
		<?php include('student/generate/quiz_table.php'); ?>
	</div>
	<div id="studRes" class="col m12">
		<!--  todo results -->
		<?php include('student/generate/results_table.php'); ?>
	</div>

	<!-- side nav -->
	<ul id="slide-out" class="side-nav fixed">
		<li>
			<div class="user-view">
				<div class="background">
					<img src="images/booktbl.jpg">
				</div>
				<img class="circle" src="images/pic2.png">
				<span class="black-text name"><?php echo $student['First_Name']. ' ' . $student['Middle_Name'][0] . '. ' . $student['Last_Name'] ; ?></span></a>
				<span class="white-text email"><?php echo $student['Section']; ?></span></a>
			</div>
		</li>
	    <li> <button data-target="modal1" class="btn modal-trigger" style="width: 300px">Change Password</button></li>
	    <li><a class="waves-effect" href="javascript:(logout());">Logout</a></li>
	</ul>

  	<!-- scripts jquery 1st -->
	<script type="text/javascript" src="js/jquery-3.2.1.min.js"></script>
	<script type="text/javascript" src="js/jquery.dataTables.min.js"></script>
	<script type="text/javascript" src="js/pagination.js"></script>
	<script type="text/javascript" src="js/materialize.js"></script>
	<script type="text/javascript" src="js/index.js"></script>
	<script type="text/javascript" src="js/student.js"></script>
</body>
</html>


//Enables deadline(the date input) when starting date is set. Cannot set starting date beyond current date.
function onSetDate() {

	setDate = document.getElementById("dateStart").value;

	if(!setDate) {

		$('#dateEnd').prop("disabled", true);
	} else if(Date.parse(setDate) > Date.now()) {

		alert("Cannot set starting date beyond current date.");
		document.getElementById("dateEnd").value = "";
		$('#dateEnd').prop("disabled", true);
		document.getElementById("dateStart").value = "";

	} else {

		$('#dateEnd').prop("disabled", false);
	}



}

$("#dateEnd").on("change", function(){
	setDate = document.getElementById("dateStart").value;
	if(Date.parse(setDate) > Date.parse($('#dateEnd').val())){
		alert("Cannot set end date before start date.");
		document.getElementById("dateEnd").value = "";
	}
});

// Enables section dropdown kung nakapili na ug subject to be uploaded
function onSetSubject() {

	$('#section').prop("disabled", false);
	$('select').material_select();

}
//restricts to input to num only
function numOnly(evt){
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode > 31 && (charCode != 46 &&(charCode < 48 || charCode > 57)))
        return false;

    return true;
}
// how the f
// $(document).ready(function() {
//     $("#durationID").keydown(function (e) {
//         // Allow: backspace, delete, tab, escape, enter and .
//         if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
//              // Allow: Ctrl/cmd+A
//             (e.keyCode == 65 && (e.ctrlKey === true || e.metaKey === true)) ||
//              // Allow: Ctrl/cmd+C
//             (e.keyCode == 67 && (e.ctrlKey === true || e.metaKey === true)) ||
//              // Allow: Ctrl/cmd+X
//             (e.keyCode == 88 && (e.ctrlKey === true || e.metaKey === true)) ||
//              // Allow: home, end, left, right
//             (e.keyCode >= 35 && e.keyCode <= 39)) {
//                  // let it happen, don't do anything
//                  return;
//         }
//         // Ensure that it is a number and stop the keypress
//         if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
//             e.preventDefault();
//         }
//     });
// });


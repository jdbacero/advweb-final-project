$("button.upload").click(function (evt) {

    evt.preventDefault();
    var file_data = $(this).parent().parent().find('input[type=file]').prop('files')[0];
    var name = $('#quizID').val();
    var subject = $('#selectSub').val();
    var section = $('#section').val();
    var duration = $('#durationID').val();
    var attempts = $('#attemptID').val();
    var start = $('#dateStart').val();
    var end = $('#dateEnd').val();
    console.log(file_data);
    console.log(name);
    console.log(start);
    console.log(end);

    if(!file_data){alert("No files selected."); return;}

    var button = $(evt.target);
    var formdata = new FormData();
    formdata.append('btn', button.attr('name'));
    formdata.append('file', file_data);
    formdata.append('quizName', name);
    formdata.append('section', section);
    formdata.append('subject', subject);
    formdata.append('duration', duration);
    formdata.append('attempts', attempts);
    formdata.append('dateStart', start);
    formdata.append('dateEnd', end);


    $.ajax({
      type: 'post',
      url: 'upload.php',
      data: formdata,
      contentType: false, //needed for input [file]
      processData: false, //needed for input [file]
      success: function (res) {
        alert(res);
        location.reload();
        }
    });

});



$('#selectSub').on('change', function() {
  var subj = $(this).val();
  $.ajax({
    method: "POST",
    url: "./instructor/generate/section.php",
    data: {"subj": subj},
    success: function(result) {
      //console.log(data);
      $('#section').html(result);
      $('select').material_select();
    },
    error: function(error){
      alert(error);
    }

  });

}); 

$('select#studSec').on('change', function() {
  var sec = $(this).val();
  $.ajax({
    method: "POST",
    url: "./instructor/generate/studSection.php",
    data: {"section": sec},
    success: function(result) {
      //console.log(data);
      $('#studPerformance').hide().html(result).show(500);
    },
    error: function(error){
      alert(error);
    }
  });
});

/* - - - Test Code - - - */
//test = new Array();
// $.ajax({
//   method: "POST",
//   url: "./adminCheckTables.php",
//   success: function(result) {
//     test = result;
//     alert(test[0]);
//   },
//   error: function(error){
//     alert(error);
//   },
//   dataType:"json"

// });

function checkEnaDis(){

  var arrayContainer = new Array();
  
  $.ajax({

    method: "POST",
    url: "./adminCheckTables.php",
    success: function(result) {
      arrayContainer = result;
      if(arrayContainer[0] == "Empty") {
        $('#tabClass, #tabStudent, #tabTeacher, #tabCourseSub, #tabSubject').addClass('disabled');
      } else {
        $('#tabClass, #tabStudent, #tabTeacher, #tabCourseSub, #tabSubject').removeClass('disabled');
      }

      if(arrayContainer[1] == "Empty") {
        $('#tabClass').addClass('disabled');  
      } else {
        $('#tabClass').removeClass('disabled');
      }
    },
    dataType:"json"

  });

}

// $('form').submit(function(e){

//   alert('aa');
//   event.preventDefault();

//   var form = $(this);

//   $.ajax({
//     method: "POST",
//     url: "upload.php",
//     data: $(this).serialize(),
//     success: function(result) {
//       //console.log(data);
//       alert(result);
//     },
//     error: function(error){
//       alert("An error has occured. Please try again.");
//     }

//   });

// });

// Variable to hold request
// var request;

// // Bind to the submit event of our form
// $("#foo").submit(function(event){

//     alert('asdf');
//     // Prevent default posting of form - put here to work in case of errors
//     event.preventDefault();

//     // Abort any pending request
//     if (request) {
//         request.abort();
//     }
//     // setup some local variables
//     var $form = $(this);

//     // Let's select and cache all the fields
//     var $inputs = $form.find("input, button");

//     // Serialize the data in the form
//     var serializedData = $form.serialize();

//     // Let's disable the inputs for the duration of the Ajax request.
//     // Note: we disable elements AFTER the form data has been serialized.
//     // Disabled form elements will not be serialized.
//     $inputs.prop("disabled", true);

//     // Fire off the request to /form.php
//     request = $.ajax({
//         url: "upload.php",
//         type: "post",
//         data: serializedData
//     });

//     // Callback handler that will be called on success
//     request.done(function (response, textStatus, jqXHR){
//         // Log a message to the console
//         console.log("Hooray, it worked!");
//         console.log($( this ).serialize());
//         alert(response);
//     });

//     // Callback handler that will be called on failure
//     request.fail(function (jqXHR, textStatus, errorThrown){
//         // Log the error to the console
//         console.error(
//             "The following error occurred: "+
//             textStatus, errorThrown
//         );
//     });

//     // Callback handler that will be called regardless
//     // if the request failed or succeeded
//     request.always(function () {
//         // Reenable the inputs
//         $inputs.prop("disabled", false);
//     });

// });

$("button.upload").click(function (evt) {

    evt.preventDefault();
    var file_data = $(this).parent().find('input[type=file]').prop('files')[0];
    console.log(file_data);

    if(!file_data){alert("No files selected."); return;}

    var button = $(evt.target);
    var formdata = new FormData();
    formdata.append('btn', button.attr('name'));
    formdata.append('file', file_data);

    var divId = $(this).parent().parent().parent().attr('id');
    console.log(divId);
    var stringy = "#"+divId+" div.adminUploadableTableContainer";
    console.log(stringy);
    

    $.ajax({
      type: 'post',
      url: 'upload.php',
      data: formdata,
      async: false,
      contentType: false, //needed for input [file]
      processData: false, //needed for input [file]
      success: function (res) {
        alert(res);
        if(button.attr('name') == 'btnUploadCourse' || button.attr('name') == 'btnUploadStudent'){
          checkEnaDis();
          console.log(res);
        }
       location.reload();

      
        // //get parent id div
        // var div = $(this).parent().parent().attr('id');
        // console.log(div);
        // $("div#"+div).parent().load(location.href+ "#"+div+">*",""); --not working
      }
    });
});
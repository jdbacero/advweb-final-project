$('.modal').modal({
  dismissible: true, // Modal can be dismissed by clicking outside of the modal
  opacity: .5, // Opacity of modal background
  inDuration: 300, // Transition in duration
  outDuration: 200, // Transition out duration
  startingTop: '4%', // Starting top style attribute
  endingTop: '10%', // Ending top style attribute
  }
);
      
$(document).ready(function(){

  $('select').material_select();

  $('ul.tabs').tabs({
    swipeable : true,
    responsiveThreshold : 1920
  });

  $('.button-collapse').sideNav( {
    menuWidth: 300, // Default is 240
    edge: 'left', // Choose the horizontal origin
    closeOnClick: false, // Closes side-nav on <a> clicks, useful for Angular/Meteor
    draggable: true // Choose whether you can drag to open on touch screens
  });

  $('.datepicker').pickadate({
  selectMonths: true, // Creates a dropdown to control month
  selectYears: 15, // Creates a dropdown of 15 years to control year,
  today: 'Today',
  clear: 'Clear',
  close: 'Ok',
  closeOnSelect: false // Close upon selecting a date,
  });


  $('#resetPW').on("click", function(){
    var newPW = $("#newPW").val();
    var oldPW = $("#oldPW").val();
    var renewPW = $("#renewPW").val();
    $.ajax({
      method: "POST",
      url: "./resetPassword.php",
      data: {"newPW": newPW,
              "oldPW": oldPW,
                "renewPW": renewPW},
      success: function(result){
        alert(result);
        if (result == "Password changed successfully.") {
          location.reload();
        }
      }
    });
  });

});

function logout() {
setTimeout(function(){window.location.href="logout.php";}, 1000);
}

document.addEventListener("DOMContentLoaded", function(){
  $('.preloader-background').delay(1700).fadeOut('slow');
  
  $('.preloader-wrapper')
    .delay(1700)
    .fadeOut();
});
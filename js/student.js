$('.takeQuiz').click(function() {

	if (confirm('Take the quiz? Take note that once you click yes it will immediately use one of your attempts available.\n\nNote: Closing the quiz page or redirecting it to another page will automatically submit your answers.')) {

		var quizId = $(this).attr('id');
		console.log(quizId);

		$.ajax( {
			type: 'post',
			url: 'quizParameters.php',
			data: {"quizId": quizId},
			success: function(){
				console.log('working');
				window.location.href = "takeQuiz.php";
			}
		})

	}

});
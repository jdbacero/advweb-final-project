var quizID;

$('.takeQuiz').click(function() {
    $('#updateQuizModal').modal('open');
    quizID = $(this).attr("id");
    var arrayContainer = new Array();
    $.ajax({
    method: "POST",
    url: "./instructor/action/getSelectedQuiz.php",
    data: {"quizID": quizID},
    success: function(result){
   		arrayContainer = result;
   		$('#uquizID').val(arrayContainer[0]);
   		$('#udateStart').val(arrayContainer[1]);
   		$('#udateEnd').val(arrayContainer[2]);
   		$('#udurationID').val(arrayContainer[3]);
   		$('#uselectSub').val(arrayContainer[6]);
   		$('#usection').val(arrayContainer[4]);
   		$('#uattemptID').val(arrayContainer[5]);
   	},
   	error: function(error){
   		alert(error);
   	},
   	dataType: "json"

   });

});

$('#uselectSub').on('change', function() {
  var subj = $(this).val();
  $.ajax({
    method: "POST",
    url: "./instructor/generate/section.php",
    data: {"subj": subj},
    success: function(result) {
      //console.log(data);
      $('#usection').html(result);
      $('#usection').material_select();
    },
    error: function(error){
      alert(error);
    }

  });

}); 

// Enables section dropdown kung nakapili na ug subject to be uploaded
function uonSetSubject() {

	$('#usection').prop("disabled", false);
	$('#usection').material_select();

}


$('#updateQuiz').on("click", function(){
	var name = $('#uquizID').val();
    var subject = $('#uselectSub').val();
    var section = $('#usection').val();
    var duration = $('#udurationID').val();
    var attempts = $('#uattemptID').val();
    var start = $('#udateStart').val();
    var end = $('#udateEnd').val();

    if(!name || !subject || !section || !duration || !attempts || !start || !end) {
    	alert("One or more fields is missing.");
    } else {
    	$.ajax({
    		method: "POST",
    		url: "./instructor/action/updateQuiz.php",
    		data: {"name": name,
    				"subject": subject,
    				"section": section,
    				"duration": duration,
    				"attempts": attempts,
    				"start": start,
    				"end": end,
    				"quizID": quizID },
    		success: function(result){
    			alert(result);
    			location.reload();
    		}
    	});
    }
});

//Enables deadline(the date input) when starting date is set. Cannot set starting date beyond current date.
function uonSetDate() {

	setDate = document.getElementById("udateStart").value;

	if(!setDate) {

		$('#udateEnd').prop("disabled", true);
	} else if(Date.parse(setDate) > Date.now()) {

		alert("Cannot set starting date beyond current date.");
		document.getElementById("udateEnd").value = "";
		$('#udateEnd').prop("disabled", true);
		document.getElementById("udateStart").value = "";

	} else {

		$('#dateEnd').prop("disabled", false);
	}



}

$("#udateEnd").on("change", function(){
	setDate = document.getElementById("udateStart").value;
	if(Date.parse(setDate) > Date.parse($('#udateEnd').val())){
		alert("Cannot set end date before start date.");
		document.getElementById("udateEnd").value = "";
	}
});

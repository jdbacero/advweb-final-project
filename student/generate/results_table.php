<?php 

$student = $_SESSION['student'];
$studentID = $student['ID'];

$query = "SELECT tblQuiz_Results.Quiz_ID, tblQuiz_Details.Quiz_Title, tblQuiz_Results.Score, (SELECT COUNT(*) AS Expr1 FROM tblQuiz_Questions WHERE (Quiz_ID = tblQuiz_Results.Quiz_ID)) AS 'Overall', tblTeacher_Details.First_Name, tblTeacher_Details.Last_Name FROM tblQuiz_Results INNER JOIN tblQuiz_Details ON tblQuiz_Results.Quiz_ID = tblQuiz_Details.Quiz_ID INNER JOIN tblTeacher_Details ON tblQuiz_Details.Teacher_ID = tblTeacher_Details.ID	WHERE tblQuiz_Results.Student_ID = $studentID";

?>

<div class="row">
  <div id="admin" class="col s12">
    <div class="material-table">
      <div class="table-header">
        <span class="table-title">Quiz Results</span>
        <div class="actions">
          <a href="#" class="search-toggle waves-effect btn-flat nopadding"><i class="material-icons">search</i></a>
        </div>
      </div>
      <table class="datatable highlight centered responsive-table">
        <thead>
          <tr>
              <th>Quiz</th>
              <th>Teacher</th>
              <th>Score</th>
              <th>Overall</th>
          </tr>
        </thead>
        <tbody>
            <?php
            $con = mysqli_connect('localhost', 'root', '', 'final_proj');
      			$result = mysqli_query($con,$query);

      			while ($row = mysqli_fetch_array($result)) {
      				echo '<td>'.$row['Quiz_Title'].'</td>';
      				echo '<td>'.$row['First_Name']. " " . $row['Last_Name'] .'</td>';
      				echo '<td>'.$row['Score'].'</td>';
      				echo '<td>'.$row['Overall'].'</td>';
      			}
                  ?>
        </tbody>
      </table>
    </div>
  </div>
</div>

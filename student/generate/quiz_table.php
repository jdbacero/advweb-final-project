<?php

$student = $_SESSION['student'];
$currentDate = date('Y/m/d');
$studentID = $student['ID'];
$section = $student['Section'];

$query = "SELECT tblQuiz_Details.Quiz_ID, tblQuiz_Details.Section, tblTeacher_Details.First_Name, tblTeacher_Details.Last_Name,  tblQuiz_Details.Subject_Code, tblQuiz_Details.Quiz_Title, tblQuiz_Details.Quiz_Available, tblQuiz_Details.Quiz_DISABLE, tblQuiz_Details.Attempts_Available - IFNULL((SELECT tblQuiz_Results.Attempts FROM tblQuiz_Results WHERE tblQuiz_Results.Quiz_ID = tblQuiz_Details.Quiz_ID AND tblQuiz_Results.Student_ID = $studentID),0) AS Attempts, tblQuiz_Details.Duration FROM tblQuiz_Details INNER JOIN tblTeacher_Details ON tblQuiz_Details.Teacher_ID = tblTeacher_Details.ID WHERE tblQuiz_Details.Section = '$section' AND '$currentDate' BETWEEN tblQuiz_Details.Quiz_Available AND tblQuiz_Details.Quiz_DISABLE";

?>

<div class="row">
  <div id="admin" class="col s12">
    <div class="material-table">
      <div class="table-header">
        <span class="table-title">Available Quizzes</span>
        <div class="actions">
          <a href="#" class="search-toggle waves-effect btn-flat nopadding"><i class="material-icons">search</i></a>
        </div>
      </div>
      <table class="datatable highlight centered responsive-table">
        <thead>
          <tr>
              <th>Section</th>
              <th>Instructor</th>
              <th>Subject</th>
              <th>Quiz</th>
              <th>Availability</th>
              <th>Expires</th>
              <th>Attempts<br>Remaining</th>
              <th>Duration<br>(Minutes)</th>
              <th></th>
          </tr>
        </thead>
        <tbody>
            <?php
            $con = mysqli_connect('localhost', 'root', '', 'final_proj');
      			$result = mysqli_query($con,$query);

      			while ($row = mysqli_fetch_array($result)) {
      				echo '<tr>';
      				echo '<td>'.$row['Section'].'</td>';
      				echo '<td>'.$row['First_Name'].'<br>'. $row['Last_Name'] .'</td>';
      				echo '<td>'.$row['Subject_Code'].'</td>';
      				echo '<td>'.$row['Quiz_Title'].'</td>';
      				echo '<td>'.date('y/m/d', strtotime($row['Quiz_Available'])).'</td>';
      				echo '<td>'.date('y/m/d', strtotime($row['Quiz_DISABLE'])).'</td>';
      				echo '<td>'.$row['Attempts'].'</td>';
      				echo '<td>'.$row['Duration'].'</td>';
      				if($row['Attempts'] == 0){
      					echo '<td>No more<br>attempts.</td>';
      				} else {
      					echo '<td class="takeQuiz" id="'. $row['Quiz_ID'] .'"><strong>Take Quiz</strong></td>';
      				}
      				echo '</tr>';
                    }
                  ?>
        </tbody>
      </table>
    </div>
  </div>
</div>
<?php include('indexheader.php') ?>

<!-- Replace this with a slider if done with everything -->

	<div class="row">
		<div class="col s6 push-m3">
			<div class="card hoverable">
				<div class="card-image">
					<img src="images/booktbl.jpg">
					<span class="card-title z-depth-2">Manage Quizzes Easily</span>
				</div>
				<div class="card-content">
					<p>Managing quizzes has never been so easy!</p>
				</div>
				<div class="card-action">
				<!-- Modal Trigger -->
				<a class="col s4 offset-s4 waves-effect waves-light btn modal-trigger" href="#modal1">Login</a> <br><br>
					<!-- Modal Structure -->
					<div id="modal1" class="modal">
						<form method="POST" action="login.php">
							<!-- modal content -->
						    <div class="modal-content" id="loginModal">
						    	<h4 align="center">Login</h4><hr/>
								<div class="input-field col s3 push-m3">
									<input id="userID" type="text" name="user" class="validate" required>
									<label for="userID">Username</label>
								</div>
								<div class="input-field col s3 push-m3">
									<input id="passID" type="password" class="validate" name="password" required>
									<label for="pasID">Password</label>
								</div>
						    </div>
						    <!-- modal footer -->
							<div class="modal-footer">
								<button class="btn waves-effect waves-light" type="submit" name="action">Sign In</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>

<!--Import jQuery before materialize.js-->
<script type="text/javascript" src="js/jquery-3.2.1.min.js"></script>
<script type="text/javascript" src="js/materialize.js"></script>
<script type="text/javascript" src="js/index.js"></script>

</html>
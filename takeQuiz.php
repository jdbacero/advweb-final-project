<?php 
	
	session_start();

	$con = mysqli_connect('localhost', 'root', '', 'final_proj');
	$quizId = $_SESSION['quizID']; 
	if($_SESSION['quizStart'] == null) {
		header("location: index.php");
	}
	//This to prevent the exploit of using the back button in order to retake the quiz even though wala nay attempts//
	$student = $_SESSION['student'];
	$studentID = $student['ID'];
	$queryRecheckAttempts = "SELECT tblQuiz_Details.Attempts_Available - IFNULL((SELECT tblQuiz_Results.Attempts FROM tblQuiz_Results WHERE tblQuiz_Results.Quiz_ID = tblQuiz_Results.Quiz_ID AND tblQuiz_Results.Student_ID = $studentID),0) AS Attempts FROM tblquiz_details WHERE tblquiz_details.Quiz_ID = $quizId";
	$RemainingAttempts = mysqli_fetch_assoc(mysqli_query($con, $queryRecheckAttempts));
	if($RemainingAttempts['Attempts'] < 1) {
		echo "<script>alert('You have used all of your attempts.');window.location.href='index.php';</script>";
	} else if ($_SESSION['lastTakenQuiz'] == true) {
		echo '<script>if(confirm("You have taken this quiz before, are you sure you want to take it again?\nKeep in mind of your remaining attempts.")){}else{header("location: index.php");}</script>';
	}
	//This to prevent the exploit of using the back button in order to retake the quiz even though wala nay attempts//
	
	//Individual IDs of question options
	$optionIDctr = 0;
	//Array of user's choice per question
	$_SESSION['userChoices'] = array();
	$choicectr = 0;
	//Array of the quiz' answers
	$_SESSION['answers'] = array();

	$query = "SELECT * FROM tblquiz_details WHERE Quiz_ID = $quizId";
	$result = mysqli_query($con, $query);
	$quiz = mysqli_fetch_assoc($result);
	$queryGetQuizQuestions = "SELECT * FROM tblquiz_questions WHERE Quiz_ID = $quizId";
	$quizQuestions = mysqli_query($con, $queryGetQuizQuestions);
?>

<!DOCTYPE html>
<html>
<head>

	<title><?php echo $quiz['Quiz_Title']; ?></title>
	<link rel="stylesheet" type="text/css" href="css/materialize.css">
	<style type="text/css">
		.slider.fullscreen {
			height: calc(100% - 70px);
		}
		footer {
			height: 70px;
			position: absolute;
			bottom: 0;
			width: 100%;
		}
		.btnNextPrev {
			color: #f2f2f2;
			z-index: 5;
			position: fixed;
			top: 40%;
			font-size: 100px;
			cursor:pointer;
			background-color: transparent;
			-moz-transition: all .1s ease-in;
    		-o-transition: all .1s ease-in;
    		-webkit-transition: all .1s ease-in;
	    	transition: all .1s ease-in;
		}
		.btnNextPrev:hover {
			font-weight: bolder;
			text-shadow: 1px 1px 1px #555;
			color: white;
		}
		#Previous {
			left: 20px;
		}

		#Next {
			right: 20px;
		}

		#submitQuiz {
			z-index: 5;
			position: fixed;
			top: 70%;
			left: calc(50% - 58.5px);
		}
	</style>
</head>
<body>
	
	<div class="btnNextPrev" id="Previous"><span><</span></div>
	<div class="btnNextPrev" id="Next"><span>></span></div>
	<div align="center" id="submitQuiz"><button class="btn waves-effect waves-light" name="action">Submit</button></div>

	<div class="slider fullscreen">


    	<ul class="slides"> 

    		<?php 

    			$questions= array();

    			//STORES ALL QUESTIONS IN THE VARIABLE ARRAY
    			while($slides = mysqli_fetch_assoc($quizQuestions)) {
    				array_push($questions, $slides);
    			}

    			//RANDOMIZES QUESTIONS
    			$shuffleKeys = array_keys($questions); //TAKES THE KEYS OF THE ARRAY ALONG WITH THEIR VALUE AND STORES THEM IN A VARIABLE
				shuffle($shuffleKeys); //SHUFFLE THE KEYS
				$randomizedQuestions = array(); //CREATE NEW VARIABLE FOR THE RANDOMIZED QUESTIONS
				foreach($shuffleKeys as $key) { //FOR EVERY KEYS THAT WERE SHUFFLED...
				    $randomizedQuestions[$key] = $questions[$key]; //SETS THE NEW ARRAY WITH THE NEWLY SHUFFLED KEYS & VALUE
				}

    			$randomizer = array_rand($questions,count($questions));
    			foreach($randomizedQuestions as $random){
    				array_push($_SESSION['answers'], $random['Correct_Answer']);
    				array_push($_SESSION['userChoices'], 'aaa'); //Placeholder for student's answers
    				echo '<li>';
    					echo '<div class="caption center-align">';
    						echo '<h3>'. $random['Questions'].'</h3>';
    						echo '<h5 class="light grey-text text-lighten-3">Choices goes here.</h5>';
    						echo '<div class="row">';
	    						echo '<div class="col s3">';
	    						echo '<input value="'. $random['Option_A'] .'" name="'. $choicectr .'" style="padding-top:3px;" type="radio" id="'. $optionIDctr .'"/><label for="'. $optionIDctr .'"></label><span style="position:fixed;">'. $random['Option_A'] .'</span>';
	    						echo '</div>';
	    						$optionIDctr++;
	    						echo '<div class="col s3">';
	    						echo '<input value="'. $random['Option_B'] .'" name="'. $choicectr .'" style="padding-top:3px;" type="radio" id="'. $optionIDctr .'"/><label for="'. $optionIDctr .'"></label><span style="position:fixed;">'. $random['Option_B'] .'</span>';
	    						echo '</div>';
	    						$optionIDctr++;
	    						echo '<div class="col s3">';
	    						echo '<input value="'. $random['Option_C'] .'" name="'. $choicectr .'" style="padding-top:3px;" type="radio" id="'. $optionIDctr .'"/><label for="'. $optionIDctr .'"></label><span style="position:fixed;">'. $random['Option_C'] .'</span>';
	    						echo '</div>';
	    						$optionIDctr++;
	    						echo '<div class="col s3">';
	    						echo '<input value="'. $random['Option_D'] .'" name="'. $choicectr .'" style="padding-top:3px;" type="radio" id="'. $optionIDctr .'"/><label for="'. $optionIDctr .'"></label><span style="position:fixed;">'. $random['Option_D'] .'</span>';
	    						echo '</div>';
	    					echo '</div>';
    					echo '</div>';
    				echo '</li>';
    				$optionIDctr++;
    				$choicectr++;
    			}

    		 ?>

    		<!-- <li> -->
        		<!-- <div class="caption center-align"> -->
          			<!-- <h3>Questions goes here!</h3> -->
          			<!-- <h5 class="light grey-text text-lighten-3">Here's our small slogan.</h5> -->
          			<!-- <input type="text" name=""> himuong radio button -->
        		<!-- </div> -->
      		<!-- </li> -->
      		<!-- <li> -->
        		<!-- <img src="https://lorempixel.com/580/250/nature/2">  --><!-- random image -->
        		<!-- <div class="caption left-align"> -->
          			<!-- <h3>Left Aligned Caption</h3> -->
          			<!-- <h5 class="light grey-text text-lighten-3">Here's our small slogan.</h5> -->
        		<!-- </div> -->
      		<!-- </li> -->
    	</ul>
	</div>

    <footer class="page-footer" style="padding-top: 0;">
        <div class="footer-copyright">
            <div class="container">
            	<h3 id="timer" style="display:inline; background-color: transparent; color: white;"></h3> <!-- Timer -->
            <a class="grey-text text-lighten-4 right" href="#!">Gwapo ko grabi</a>
            </div>
          </div>
    </footer>
</body>

<!--Import jQuery before materialize.js-->
<script src="js/jquery-3.2.1.min.js"></script>
<script src='js/materialize.min.js'></script>
<script type="text/javascript">
	console.log("test");

	$(document).ready(function(){

    	$('.slider').slider();

		//DILI MU WORK SA LATEST BROWSERS ANG ALERT SA SULOD SA WINDOW.UNLOAD!!   :-(
		$(window).on("unload", function() {
		  	submitQuiz();
		});
// $(window).on('beforeunload', function(){
//       return 'Are you sure you want to leave?';
// });

		//--------------------//
		$('.slider').slider('next');//temporary fix
		$('.slider').slider('prev');//for a
		$('.slider').slider('pause');//particular problem
		//--------------------//

		$('.indicator-item').on('click',function(){
		    $('.slider').slider('pause');
		});

		$('.slider').on('swipeLeft', function() {alert('test');});

		//Sets timer duration
		var timer = <?php echo $quiz['Duration'] ?> * 60;

		//Timer
		var x = setInterval(function() {

		    // Time calculations for minutes and seconds
		    var minutes = Math.floor(timer/60);
		    var seconds = Math.floor(timer % 60);
		    
		    // Output the result in an element with id="timer"
		    document.getElementById("timer").innerHTML = minutes + "m " + seconds + "s ";
		    
		    // If the count down is over, do this
		    if (timer < 0) {
		        clearInterval(x);
		        // document.getElementById("timer").innerHTML = "EXPIRED";
		        document.getElementById("timer").innerHTML = 'Time\'s Up';
		        submitQuiz();
		        alert("Time's up! \nYour answers are automatically submitted.");
		    }
		    
		    timer--;
		}, 1000);
    })

	$('input:radio').on("click", function(){
	
		var thisRadiobutton = $(this).attr("name");
		// $userChoices[thisRadiobutton] = $('input:radio['+ thisRadiobutton +']').val();
		// alert($userChoices[thisRadiobutton]);
		// var userAnswer = $('input:radio['+ thisRadiobutton +']').val();
		var userAnswer = $('input[name=' + thisRadiobutton + ']:checked').val();

		$.ajax({

			method: "POST",
			url: "./userAnswer.php",
			data: {"rdoAnswer": userAnswer,
					"questionNo": thisRadiobutton},
			success: function() {
				
			}

		});

	});


	
	// // next & preview botons
	$('.btnNextPrev').on("click", function() {
		var btnpressed = $(this).attr("id");
		console.log(btnpressed);
		if(btnpressed == "Next") {
			$('.slider').slider('next');
			$('.slider').slider('pause');
		} 
		else {
			$('.slider').slider('prev');
			$('.slider').slider('pause');
		}
	});

	$('#submitQuiz').on("click", function(){
		if (confirm("You are about to submit your answers. Are you sure?")) {
			submitQuiz();
		}
	});
	
	//Checks answer
	function submitQuiz() {
        $.ajax({

			method: "POST",
			url: "./checkAnswer.php",
			async: false,
			success: function(result) {
				alert(result);
				window.location.href="index.php";
			}

		});
	}

</script>
</html>
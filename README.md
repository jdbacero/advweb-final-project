College final project Advanced Web back in 2017 - 2018.

Allows teachers to upload an excel file for subjects, students, sections, and quizzes. Students can then take the quiz.

This was done in PHP5, so please that in mind that there will be errors if you use a newer PHP version.

<?php include('indexheader.php') ?>

	<div class="row">
		<div class="col s12 m6 offset-m3">
			<div class="card hoverable">
				<div class="card-image">
					<!-- ilisi ang image as contact.jpg after the laughs -->
					<img src="images/pexels-photo-373076.jpeg" style="height: 300px;">
					<span class="card-title" style="font-size: 40px; color: gray;">Contact us</span>
				</div>
				<div class="card-content white text">
					<form method="POST">
						<div class="row">
							<!-- name -->
							<div class="input-field col m7">
								<input type="text" id="nameID" name="name" class="validate" required>
								<label for="nameID">Name</label>
							</div>
							<div class="col m4 offset-m1">
								<span style="font-weight: bold;">Our contact information</span><br><br>
								<span><i class="material-icons left">location_on</i> Taga San Fernando</span>
							</div>
							<!-- email -->
							<div class="input-field col m7">
								<input type="text" id="emailID" name="email" class="validate" required>
								<label for="emailID">Email</label>
							</div>
							<div class="col m4 offset-m1">
								<span><i class="material-icons left">phone_android</i> 0932 337 6357</span>
								<span style="display: block; margin-top: 15px;"><i class="material-icons left">mail</i>jdbacero@gmail.com</span>
							</div>
							<!-- message -->
							<div class="input-field col m7" style="padding-left: 0px;">
								<div class="input-field col m12">
									<textarea id="messageID" class="materialize-textarea"></textarea>
									<label for="messageID">Message</label>
								</div>
							</div>
							<!-- warakoy lingaw ani i comment lang haahahah -->
							<div class="col m4 offset-m1">
								<span>_____________________________</span><br>
								<a href="https://www.facebook.com/jdbacero"><img style="width: 10%; margin-left: 40px;" src="images/fb.png"></a>
								<a href="https://twitter.com/jdbacero"><img style="width: 10%;" src="images/twitter.png"></a>
								<a href="https://plus.google.com/u/0/107430895868634081729"><img style="width: 11%;" src="images/google.png"></a>
								<a href="https://i.imgur.com/xlobgrF.png"><img style="width: 11%;" src="images/instagram.png"></a>
							</div>
							<!-- send -->
							<div class="col m12">
								<button class="btn waves-effect waves-light" type="submit" name="action" style="position: relative; left: 20%; margin-top: 20px;">Send <i class="material-icons right">send</i></button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
	<!-- una jquery -->
	<script type="text/javascript" src="js/jquery-3.2.1.min.js"></script>
	<script type="text/javascript" src="js/materialize.js"></script>
</body>
</html>
<!-- this is the header for index only -->

<?php
session_start();
if(isset($_SESSION['username'])) {

  $userType = $_SESSION['usertype'];

  switch ($userType) {
      case 'Admin':
        header('location: admin_home.php');       
        break;

      case 'Instructor':
        header('location: instructor_home.php');   
        break;
      
      default:
        header('location: student_home.php');   
        break;
    }
} else {
  session_destroy();
}

?>

<!DOCTYPE html>
<html>
<head>
	<link rel="stylesheet" type="text/css" href="css/materialize.css">
  <link rel="stylesheet" type="text/css" href="css/style.css">
	<link rel="stylesheet" type="text/css" href="icon/icon.css">
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<meta charset="utf-8">
	<title>Login</title>
</head>
<body>

  <nav>
    <div class="nav-wrapper brown lighten-1">
      <a href="index.php" class="brand-logo right"><span class="z-depth-2 brown darken-3 quiz">Quiz Management System</span></a>
      <ul id="nav-mobile" class="left hide-on-med-and-down">
        <li><a href="about.php">About</a></li>
        <li><a href="contact_us.php">Contact</a></li>
      </ul>
    </div>
  </nav>
        
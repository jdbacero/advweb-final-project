<div class="row">
  <div id="admin" class="col s12">
    <div class="material-table">
      <div class="table-header">
        <span class="table-title">Students</span>
        <div class="actions">
          <a href="#" class="search-toggle waves-effect btn-flat nopadding"><i class="material-icons">search</i></a>
        </div>
      </div>
      <table class="datatable highlight centered responsive-table">
        <thead>
          <tr>
              <th>ID</th>
              <th>Section</th>
              <th>Course</th>
              <th>First Name</th>
              <th>Middle Name</th>
              <th>Last Name</th>
              <th>Username</th>
              <th>User Type</th>
          </tr>
        </thead>
        <tbody>
            <?php
              $con = mysqli_connect('localhost', 'root', '', 'final_proj');
              $query = "SELECT ID, Section, Course_ID, First_Name, Middle_Name, Last_Name, userName, userType FROM tblStudents_Details";

              $result = mysqli_query($con,$query);

              while ($row = mysqli_fetch_array($result)) {
                echo '<tr>';
                echo '<td>'.$row['ID'].'</td>';
                echo '<td>'.$row['Section'].'</td>';
                echo '<td>'.$row['Course_ID'].'</td>';
                echo '<td>'.$row['First_Name'].'</td>';
                echo '<td>'.$row['Middle_Name'].'</td>';
                echo '<td>'.$row['Last_Name'].'</td>';
                echo '<td>'.$row['userName'].'</td>';
                echo '<td>'.$row['userType'].'</td>';
                echo '</tr>';
              }
            ?>
        </tbody>
      </table>
    </div>
  </div>
</div>
<div class="row">
  <div id="admin" class="col s12">
    <div class="material-table">
      <div class="table-header">
        <span class="table-title">Manage Classes</span>
        <div class="actions">
          <a href="#" class="search-toggle waves-effect btn-flat nopadding"><i class="material-icons">search</i></a>
        </div>
      </div>
      <table class="datatable highlight centered responsive-table">
        <thead>
          <tr>
              <th>Section</th>
              <th>Course</th>
          </tr>
        </thead>
        <tbody>
            <?php
            $con = mysqli_connect('localhost', 'root', '', 'final_proj');
			$query = 'SELECT * FROM tblclass;';
			$result = mysqli_query($con,$query);

			while ($row = mysqli_fetch_array($result)) {
				echo '<tr>';
				echo '<td>'.$row['Section'].'</td>';
				echo '<td>'.$row['Course'].'</td>';
				echo '</tr>';
              }
            ?>
        </tbody>
      </table>
    </div>
  </div>
</div>
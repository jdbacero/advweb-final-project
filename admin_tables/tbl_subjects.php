<div class="row">
  <div id="admin" class="col s12">
    <div class="material-table">
      <div class="table-header">
        <span class="table-title">Manage Subjects</span>
        <div class="actions">
          <a href="#" class="search-toggle waves-effect btn-flat nopadding"><i class="material-icons">search</i></a>
        </div>
      </div>
      <table class="datatable highlight centered responsive-table">
        <thead>
          <tr>
              <th>Subject Name</th>
              <th>Subject Code</th>
          </tr>
        </thead>
        <tbody>
            <?php
            $con = mysqli_connect('localhost', 'root', '', 'final_proj');
			$query = 'SELECT * FROM tblsubjects;';
			$result = mysqli_query($con,$query);

			while ($row = mysqli_fetch_array($result)) {
				echo '<tr>';
				echo '<td>'.$row['Subject_Name'].'</td>';
				echo '<td>'.$row['Subject_Code'].'</td>';
				echo '</tr>';
              }
            ?>
        </tbody>
      </table>
    </div>
  </div>
</div>
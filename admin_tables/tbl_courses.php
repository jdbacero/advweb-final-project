<div class="row">
  
  <div id="admin" class="col s12">
    <div class="material-table">
      <div class="table-header">
        <span class="table-title">Manage Courses</span>
        <div class="actions">
          <a href="#" class="search-toggle waves-effect btn-flat nopadding"><i class="material-icons">search</i></a>
        </div>
      </div>
      <table class="datatable highlight centered responsive-table">
        <thead>
          <tr>
              <th>Course ID</th>
              <th>Course Name</th>
          </tr>
        </thead>
        <tbody>
            <?php
            $con = mysqli_connect('localhost', 'root', '', 'final_proj');
			$query = 'SELECT * FROM tblcourse;';
			$result = mysqli_query($con,$query);

			while ($row = mysqli_fetch_array($result)) {
				echo '<tr>';
				echo '<td>'.$row['Course_ID'].'</td>';
				echo '<td>'.$row['Course_Name'].'</td>';
				echo '</tr>';
              }
            ?>
        </tbody>
      </table>
    </div>
  </div>
</div>
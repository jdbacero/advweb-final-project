<div class="row">
  <div id="admin" class="col s12">
    <div class="material-table">
      <div class="table-header">
        <span class="table-title">User Logs</span>
        <div class="actions">
          <a href="#" class="search-toggle waves-effect btn-flat nopadding"><i class="material-icons">search</i></a>
        </div>
      </div>
      <table class="datatable highlight centered responsive-table">
        <thead>
          <tr>
              <th>Username</th>
              <th>Time In</th>
              <th>Time Out</th>
          </tr>
        </thead>
        <tbody>
            <?php
              $con = mysqli_connect('localhost', 'root', '', 'final_proj');
              $query = 'SELECT * FROM tbluser_logs;';
              $result = mysqli_query($con,$query);

              while ($row = mysqli_fetch_array($result)) {
                echo '<tr>';
                echo '<td>'.$row['userName'].'</td>';
                echo '<td>'.$row['TimeIn'].'</td>';
                echo '<td>'.$row['TimeOut'].'</td>';
                echo '</tr>';
              }
            ?>
        </tbody>
      </table>
    </div>
  </div>
</div>
<?php

#Connects to the server
$con = mysqli_connect('localhost', 'root', '', 'final_proj');

#gets the pressed uploaded file
$btn = $_POST['btn'];

#stores filepath
$location = $_FILES["file"]["tmp_name"];

#Uses the required class for uploading excel - PHPEXCEL
include ("Classes/PHPExcel/IOFactory.php");

#Separate PHP file for uploading functions/checkers
include("upload_Func.php");

if($btn == 'btnUploadCourse') {

	#Instantiates PHPEXCEL IOFactory
	$objPHPExcel = PHPExcel_IOFactory::load($location);

	#Gets the highest letter of column in the uploaded excel and converts it to integer 
	$highestColumn = $objPHPExcel->setActiveSheetIndex(0)->getHighestColumn();
	$highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn);

	#Gets the highest index of row in the uploaded excel 
	$highestRowIndex = $objPHPExcel->setActiveSheetIndex(0)->getHighestRow();

	#Checks if no files is selected

	$headers = array("Course ID", "Course Name");

	if(contentChecker(count($headers), $highestColumnIndex, $headers, $highestRowIndex) == true) {
		$increment = 2;
		uploadExcel($headers, $highestRowIndex, $con, $increment, 'tblCourse', 2);
	}
}


if($btn == 'btnUploadStudent') {

	#Instantiates PHPEXCEL IOFactory
	$objPHPExcel = PHPExcel_IOFactory::load($location);

	#Gets the highest letter of column in the uploaded excel and converts it to integer 
	$highestColumn = $objPHPExcel->setActiveSheetIndex(0)->getHighestColumn();
	$highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn);

	#Gets the highest index of row in the uploaded excel 
	$highestRowIndex = $objPHPExcel->setActiveSheetIndex(0)->getHighestRow();
	$headers = array("Section", "First Name", "Middle Name", "Last Name", "Course");

	if(contentChecker(count($headers), $highestColumnIndex, $headers, $highestRowIndex) == true) {
		$increment = 2;
		uploadStudents($highestRowIndex, $con);
	}

}

if($btn == 'btnUploadTeacher') {

	#Instantiates PHPEXCEL IOFactory
	$objPHPExcel = PHPExcel_IOFactory::load($location);

	#Gets the highest letter of column in the uploaded excel and converts it to integer 
	$highestColumn = $objPHPExcel->setActiveSheetIndex(0)->getHighestColumn();
	$highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn);

	#Gets the highest index of row in the uploaded excel 
	$highestRowIndex = $objPHPExcel->setActiveSheetIndex(0)->getHighestRow();
	$headers = array("First Name", "Middle Name", "Last Name");

	if(contentChecker(count($headers), $highestColumnIndex, $headers, $highestRowIndex) == true) {
		uploadTeachers($highestRowIndex, $con);
	}

}

if($btn == 'btnUploadSubject') {

	#Instantiates PHPEXCEL IOFactory
	$objPHPExcel = PHPExcel_IOFactory::load($location);

	#Gets the highest letter of column in the uploaded excel and converts it to integer 
	$highestColumn = $objPHPExcel->setActiveSheetIndex(0)->getHighestColumn();
	$highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn);

	#Gets the highest index of row in the uploaded excel 
	$highestRowIndex = $objPHPExcel->setActiveSheetIndex(0)->getHighestRow();
	$headers = array("Subject Name", "Subject Code");

	if(contentChecker(count($headers), $highestColumnIndex, $headers, $highestRowIndex) == true) {
		$increment = 2;
		uploadExcel($headers, $highestRowIndex, $con, $increment, 'tblsubjects', 2);
	}

}

if($btn =='btnUploadCourseSubject') {

	#Instantiates PHPEXCEL IOFactory
	$objPHPExcel = PHPExcel_IOFactory::load($location);

	#Gets the highest letter of column in the uploaded excel and converts it to integer 
	$highestColumn = $objPHPExcel->setActiveSheetIndex(0)->getHighestColumn();
	$highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn);

	#Gets the highest index of row in the uploaded excel 
	$highestRowIndex = $objPHPExcel->setActiveSheetIndex(0)->getHighestRow();
	$headers = array("Subject", "Course");

	if(contentChecker(count($headers), $highestColumnIndex, $headers, $highestRowIndex) == true) {
		$increment = 2;
		uploadExcel($headers, $highestRowIndex, $con, $increment, 'tblcourse_subjects', 2);
	}

}

if($btn == 'btnUploadQuiz') {
	
	#Instantiates PHPEXCEL IOFactory
	$objPHPExcel = PHPExcel_IOFactory::load($location);

	#Gets the highest letter of column in the uploaded excel and converts it to integer 
	$highestColumn = $objPHPExcel->setActiveSheetIndex(0)->getHighestColumn();
	$highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn);

	#Gets the highest index of row in the uploaded excel 
	$highestRowIndex = $objPHPExcel->setActiveSheetIndex(0)->getHighestRow();
	$headers = array("Questions", "Option A", "Option B", "Option C", "Option D", "Correct Answer");

	if(empty($_POST['quizName']) || empty($_POST['section']) || empty($_POST['subject']) || empty($_POST['duration']) || empty($_POST['attempts']) || empty($_POST['dateStart']) || empty($_POST['dateEnd'])){

		echo 'One or more inputs is empty. Please fill all inputs.';
		return;

	}
	#Put all quiz details in array
	$quizDetails = array (

		$_POST['quizName'], //0
		$_POST['section'],//1
		$_POST['subject'],//2
		$_POST['duration'],//3
		$_POST['attempts'],//4
		$_POST['dateStart'],//5
		$_POST['dateEnd']//6

	);

	if(contentChecker(count($headers), $highestColumnIndex, $headers, $highestRowIndex) == true) {
		uploadQuiz($quizDetails, $con, $highestRowIndex);
	}
	echo 'Upload successful.';

}

?>
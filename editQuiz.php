<?php

$con = mysqli_connect('localhost', 'root', '', 'final_proj');
$subject = array();
#isud ug array para gamiton sa dropdown/select as items nila

$query = "SELECT * FROM tblsubjects ORDER BY Subject_Code ASC";
$result = mysqli_query($con, $query);

while($data = mysqli_fetch_array($result)) {
	array_push($subject, $data['Subject_Code']);
}

?>

<div id="updateQuizModal" class="modal">
	<div class="modal-content">
		<h4 align="center">Update Quiz</h4><hr/>
		<div class="row">
			<!-- select quiz -->
				<!-- quiz name -->
				<div class="input-field col s12">
					<input id="uquizID" name="quizName" type="text" class="validate" required>
					<label for="uquizID">Quiz Name</label>
				</div>
			  	<!-- quiz subject -->
			  	<div class="input-field col s6">
			    	<select name="subject" id="uselectSub" onchange="uonSetSubject()">
			      		<option value="" disabled selected>Subject</option>
						<!-- <option value="1">Option 1</option>
			      		<option value="2">Option 2</option>
			      		<option value="3">Option 3</option> -->
			      		<?php

							foreach($subject as $arr) {
								echo "<option>$arr</option>";	
							}
			      			
			      		?>
			    	</select>
			  	</div>
				<!-- quiz section -->
				<div class="input-field col s6">
			    	<select name="section" id="usection">
			      		<option value="" disabled selected>Section</option>
			      		<option id="uselectPlaceholder" class="sectionThing">Placeholder</option>
			      		

			    	</select>
			  	</div>
				<!-- quiz duration -->
			  	<div class="input-field col s6">
					<input id="udurationID" type="text" class="validate" name="duration" required onkeypress="return isNumberKey(event)">
					<label for="udurationID">Duration (minutes)</label>
				</div>
				<!-- quiz attempts  -->
				<div class="input-field col s6">
					<input id="uattemptID" type="text" name="attempts" class="validate" required onkeypress="return isNumberKey(event)"> 
					<label for="uattemptID">Attempts Available</label>
				</div>
				<!-- quiz availability -->
				<div class="col s6">
					<input type="text" class="datepicker" id="udateStart" name="dateStart" placeholder="Start Availability" onchange="uonSetDate()" >
				</div>
				<!-- quiz deadline -->
				<div class="col s6">
					<input type="text" class="datepicker" id="udateEnd" name="dateEnd" placeholder="Deadline">
				</div>
				<!-- quiz upload -->
				<div class="col s12">
					<button class="update waves-effect waves-light btn col s2 offset-s5" name="btnUpdateQuiz" id="updateQuiz">Update</button>
				</div>	
		</div>
	</div>
</div>